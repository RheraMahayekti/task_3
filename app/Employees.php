<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = "employees";
    protected $fillable = ['idnumber','fullname','gender', 'religion','bloodtype','address','mobile','email'];
 
    public function jobdesk()
    {
        return $this->hasMany('App\EmployeesJobdesk','employees_id','id');
    }
}
