<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobdesk extends Model
{
    protected $table = "jobdesk";
    protected $fillable = ['id','jobdesk'];
    public $timestamps = false;
}
