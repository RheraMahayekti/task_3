<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Proengsoft\JsValidation\JsValidator;
use Yajra\DataTables\DataTables;
use App\Employees;

class EmployeeController extends Controller
{
    protected $validationRules=[
        'fullname'      =>'required',
        'mobile'        =>'required',
        'email'         =>'required'
    ];

    public function getDataview(Request $request){
        if ($request->type=="dataview"){
            $data = Employees::all();
            return datatables()->of($data)
                    ->addColumn('jobdesk',function($data){
                        $str ='';
                        $no = 1;
                        foreach($data->jobdesk as $jobdesk){
                            $str .= $no.'. '.$jobdesk->jobdesk->jobdesk.'</br>';$no++;
                        }
                        return $str;

                    })
                    ->addColumn('action', function($data){
                        return '<a href = "edit/'.$data->id.'" class = "btn btn-xs btn-primary">Edit</a>
                                <a href = "delete/'.$data->id.'" class = "btn btn-xs btn-primary">Delete</a>';
                    })
                    ->rawColumns(['jobdesk', 'action'])
                    ->make(true);
        }
        return view('Employee-view');
    }
    public function store(Request $request){
        $input = $request->all();
        // $messages = [
        //     'idnumber.required' => 'ID Number is not correct!!'
        // ];
        $v = \Validator::make($input, $this->validationRules);
            

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->messages());
        }
        else
        {
            if($input['is_update'] !=0){
                
                $employees = Employees::find($input['is_update']);
                $employees->jobdesk()->delete();
                if(isset($input['jobdesk_id'])){
                    foreach($input['jobdesk_id'] as $key=>$jobdesk){
                        $employees->jobdesk()->create(['jobdesk_id'=>$jobdesk]);
                    }
                    unset($input['jobdesk_id']);
                }
                $employees->update($input);
                $employees->save();
                // flash()->success('The employee has been updated successfully.');

            }else{
                $input['jobdesk_id'] = $request->input('jobdesk_id');

               $save =  Employees::create($input);
               if(isset($input['jobdesk_id'])){
                    foreach($input['jobdesk_id'] as $key=>$jobdesk){

                        \App\EmployeesJobdesk::create([
                            'jobdesk_id'=>$jobdesk,
                            'employees_id'=>$save->id
                            ]);
                    }
                    unset($input['jobdesk_id']);
                }
               
                // flash()->success('The employee has been created successfully.');
            }
            return redirect ('employee/view');

        }
    }
    public function dataedit($id){
        $edit = Employees::find($id);
        return view('Employee-edit')->with(compact('edit'));
    }
    public function datadelete($id){
        $edit = Employees::find($id)->delete();
        return redirect ('employee/view');
    }
}