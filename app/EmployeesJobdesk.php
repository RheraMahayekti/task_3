<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeesJobdesk extends Model
{
    protected $table = "employees_jobdesk";
    protected $fillable = ['employees_id','jobdesk_id'];
    public $timestamps = false;

    public function jobdesk(){
        return $this->belongsTo('App\Jobdesk', 'jobdesk_id','id');
    }

    
}
