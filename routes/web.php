<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/employee/view', 'EmployeeController@getDataview')->name('data.view');
Route::view('/employee/add', 'employee-edit');
Route::post('/employee/process', 'EmployeeController@store');
Route::get('/employee/edit/{id}', 'EmployeeController@dataedit');
Route::get('/employee/delete/{id}', 'EmployeeController@datadelete');