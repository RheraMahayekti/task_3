<!doctype html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link type="text/css" href="/css/app.css" rel="stylesheet">
    <link type="text/css" href="/css/apps.css" rel="stylesheet">
    <link type="text/css" href="/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/images/icons/css/font-awesome.css" rel="stylesheet">
</head>
<body>

    <main class="py-4">
        <div class="span9">
            
    <div class="module-head">
		<h3>Employees Form</h3>
	</div>
	<div class="module-body">
	@if(count($errors) > 0)
		<div class="alert alert-danger" id="danger-alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<ul>
				@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
    @endif
		<br><br>
		<form action="/employee/process" method="POST" enctype="multipart/form-data" id="form-input" class="form-horizontal row-fluid">
			{{csrf_field()}}
			<input type="hidden" value="{{isset($edit)?$edit->id:'0'}}" name="is_update">
            <div class="control-group">
				<label class="control-label" for="basicinput">FULL NAME </label>
				<div class="controls">
					<input type="text" required id="basicinput" placeholder="Insert Full Name" class="span8" value="{{isset($edit)?$edit->fullname:''}}" name="fullname">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="basicinput">GENDER</label>
				<div class="controls">
                    <select tabindex="1" data-placeholder="Pilih Salah Satu" value="{{isset($edit)?$edit->gender:''}}"name="gender">
            			<option value="male">MALE</option>
						<option value="female">FEMALE</option>
                    </select>
                </div>
			</div>
            <div class="control-group">
				<label class="control-label" for="basicinput">RELIGION</label>
				<div class="controls">
					<select tabindex="#" data-placeholder="Choose One" class="span8" value="{{isset($edit)?$edit->religion:''}}" name="religion">
						<option value="Hinduism" {{isset($edit)?($edit->religion === 'Hinduism' ? 'selected' : ''):''}}>HINDUISM</option>
						<option value="Islam" {{isset($edit)?($edit->religion === 'Islam' ? 'selected' : ''):''}}>ISLAM</option>
                        <option value="Christian" {{isset($edit)?($edit->religion === 'Christian' ? 'selected' : ''):''}}>CHRISTIAN</option>
                        <option value="Buddhism" {{isset($edit)?($edit->religion === 'Buddhism' ? 'selected' : ''):''}}>BUDDHISM</option>
                        <option value="Other" {{isset($edit)?($edit->religion === 'Other' ? 'selected' : ''):''}}>OTHER</option>
					</select>
				</div>
			</div>
            
            <div class="control-group">
				<label class="control-label" for="basicinput">BLOOD TYPE</label>
				<div class="controls">
					<select tabindex="#" data-placeholder="Choose One" class="span8" value="{{isset($edit)?$edit->bloodtype:''}}" name="bloodtype">
						<option value="A" {{isset($edit)?($edit->bloodtype === 'A' ? 'selected' : ''):''}}>A</option>
						<option value="B" {{isset($edit)?($edit->bloodtype === 'B' ? 'selected' : ''):''}}>B</option>
                        <option value="O" {{isset($edit)?($edit->bloodtype === 'O' ? 'selected' : ''):''}}>O</option>
                        <option value="AB" {{isset($edit)?($edit->bloodtype === 'AB' ? 'selected' : ''):''}}>AB</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="basicinput">ADDRESS</label>
				<div class="controls">
                    <input type="text" id="basicinput" placeholder="Insert Address" class="span8" value="{{isset($edit)?$edit->address:''}}" name="address">
				</div>
			</div>
            <div class="control-group">
				<label class="control-label" for="basicinput">MOBILE </label>
				<div class="controls">
					<input type="text" id="basicinput" placeholder="Insert Mobile Number" class="span8" value="{{isset($edit)?$edit->mobile:''}}" name="mobile">
				</div>
			</div>
            <div class="control-group">
				<label class="control-label" for="basicinput">EMAIL </label>
				<div class="controls">
					<input type="text" id="basicinput" placeholder="Insert Email" class="span8" value="{{isset($edit)?$edit->email:''}}" name="email">
				</div>
			</div>
            <div class="control-group">
				<label class="control-label" for="basicinput">JOB DESK</label>
				<div class="controls">
                @php
				if(isset($edit)){
					$jobdesk= $edit->jobdesk()->pluck('jobdesk_id')->toArray();
				}else{
					$jobdesk = [];
				}
				@endphp
                    <div class="checkbox">
					@php($jobdesks = \App\Jobdesk::all())
					@foreach($jobdesks as $j)
                        <label>
                            <input type="checkbox" value="{{$j->id}}" name="jobdesk_id[]" {{in_array($j->id,$jobdesk) ? 'checked' : '' }}> {{$j->jobdesk}} 
                        </label>
					@endforeach
                    </div>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button name="submit" type="submit" class="btn">Submit Form</button>
				</div>
			</div>
                                        
		</form>
		<a href="/employee/view"><button class="btn">Back</button></a>
	</div>
    
    </div>
    </main>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
	<script>
	$("#form-input").validate();
	</script>
</body>
</html>