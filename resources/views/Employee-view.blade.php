<!doctype html>
<head>
   <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    
    <link type="text/css" href="/css/app.css" rel="stylesheet">
    <link type="text/css" href="/css/apps.css" rel="stylesheet">
    <link type="text/css" href="/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href="{{ url('https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css') }}">
</head>
<body>

    <a class="btn btn-success float-right" href="add" id="newData"> Create New Data</a>
    <div class="module-body table">
    <table id="datatable" cellpadding="0" cellspacing="0" class="display table table-responsive " width="100%">
        <thead>
            <tr>
                <th> NO </th>
                <th> FULL NAME </th>
                <th> GENDER </th>
                <th> RELIGION </th>
                <th> BLOOD TYPE </th>
                <th > ADDRESS </th>
                <th style="min-width:150px"> MOBILE </th>
                <th> EMAIL </th>
                <th style="min-width:200px"> JOB DESK </th>
                <th>  </th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>

    <script src="{{ url('https://code.jquery.com/jquery-3.5.1.js') }}"></script>
    <script src="{{ url('https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                processing  : true,
                serverSide  : true,
                ajax        : "{{route('data.view')}}?type=dataview",
                columns: [
                    {data   : 'id',          name  : 'id'},
                    {data   : 'fullname',    name  : 'fullname'},
                    {data   : 'gender',      name  : 'gender'},
                    {data   : 'religion',    name  : 'religion'},
                    {data   : 'bloodtype',   name  : 'bloodtype'},
                    {data   : 'address',     name  : 'address'},
                    {data   : 'mobile',      name  : 'mobile'},
                    {data   : 'email',       name  : 'email'},
                    {data   : 'jobdesk',     name  : 'jobdesk', orderable : false, searchable : false},
                    {data   : 'action',      name  : 'action', orderable : false, searchable : false}
                ],
                order       : [[0, 'asc']],
            });
        })
    </script>
    
  
</body>
</html>