<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function(Blueprint $table){
            $table->increments('id');
            $table->string('fullname');
            $table->string('gender');
            $table->string('religion');
            $table->string('bloodtype');
            $table->string('address');
            $table->string('mobile');
            $table->string('email');
            $table->timestamps();
        });
        Schema::create('jobdesk', function(Blueprint $table){
            $table->increments('id');
            $table->string('jobdesk');
        });
        Schema::create('employees_jobdesk', function(Blueprint $table){
            $table->increments('id');
            $table->integer('employees_id')->unsigned();
            $table->foreign('employees_id')->references('id')->on('employees')->onDelete('cascade');
            $table->integer('jobdesk_id')->unsigned();
            $table->foreign('jobdesk_id')->references('id')->on('jobdesk')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
        Schema::dropIfExists('employees_jobdesk');
    }
}
