<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class NewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        DB::table('jobdesk')->insert([
        [
            'id'=>1,
            'jobdesk'=>'Create Documents'
        ],[
            'id'=>2,
            'jobdesk'=>'Problem Analysis'
        ],[
            'id'=>3,
            'jobdesk'=>'Code'
        ],[
            'id'=>4,
            'jobdesk'=>'Manage Finances'
        ],[
            'id'=>5,
            'jobdesk'=>'Product Marketing'
        ]
        ]);
        
        for($i=0; $i<20; $i++)
        {
            $gender = $faker->randomElement(['male', 'female']);
            DB::table('employees')->insert([
                'fullname'      =>$faker->name($gender),
                'gender'        =>$gender,
                'religion'      =>$faker->randomElement(['Hinduism', 'Islam', 'Buddhism', 'Christian', 'Other']),
                'bloodtype'     =>$faker->randomElement(['A', 'B', 'O', 'AB']),
                'address'       =>$faker->address,
                'mobile'        =>$faker->phoneNumber,
                'email'         =>$faker->email,
            ]);
            $array_jobdesk = [1,2,3,4,5];
            for ($j=1; $j <= rand(1,5) ; $j++) { 
                $jobdesk_id = $array_jobdesk[array_rand($array_jobdesk)];
                $remove_array = array_search($jobdesk_id,$array_jobdesk);
                unset($array_jobdesk[$remove_array]);
                DB::table('employees_jobdesk')->insert([
                    'employees_id'  =>$i+1,
                    'jobdesk_id'    =>$jobdesk_id,
                ]);
            }
        }
    }
}
